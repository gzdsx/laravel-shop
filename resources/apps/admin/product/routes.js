import ProductList from "./ProductList";
import ProductEdit from "./ProductEdit";
import FreightTemplateList from "./FreightTemplateList";
import FreightTemplateEdit from "./FreightTemplateEdit";
import ProductModel from "./ProductModel";
import ProductAttrValue from "./ProductAttrValue";
import ProductCategory from "./ProductCategory";
import RefundAddress from "./RefundAddress";
import RefundReason from "./RefundReason";
import Coupon from "./Coupon";


module.exports = [
    {path: '/product/list', component: ProductList, meta: {title: '商品管理'}},
    {path: '/product/edit/:id?', component: ProductEdit, meta: {title: '编辑商品'}},
    {path: '/product/category', component: ProductCategory, meta: {title: '商品分类'}},
    {path: '/freight-template/list', component: FreightTemplateList, meta: {title: '运费模板'}},
    {path: '/freight-template/edit/:template_id?', component: FreightTemplateEdit, meta: {title: '编辑模板'}},
    {path: '/refund/address', component: RefundAddress, meta: {title: '退货地址'}},
    {path: '/refund/reason', component: RefundReason, meta: {title: '退货理由'}},
    {path: '/coupon/list', component: Coupon, meta: {title: '优惠券管理'}},
    {path: '/product/model', component: ProductModel, meta: {title: '型号管理'}},
    {path: '/product/attr-value', component: ProductAttrValue, meta: {title: '商品型号'}},
]
