import ApiService from "./ApiService";

const UserService = {
    listUsers(params) {
        return ApiService.get('/users', {params});
    },
    getUser(uid) {
        return ApiService.get('/users/'+uid);
    },
    storeUser(user) {
        return ApiService.post('/users', {user});
    },
    deleteUser(ids) {
        return ApiService.post('/users/batch-delete', {ids});
    },
    batchUpdate(ids, data) {
        return ApiService.post('/user/batch-update', {ids, data});
    },
    listGroups(params) {
        return ApiService.get('/user-groups', {params});
    },
    getGroup(gid) {
        return ApiService.get('/user-groups/'+gid);
    },
    storeGroup(group) {
        return ApiService.post('/user-groups', {group});
    },
    deleteGroup(ids) {
        return ApiService.post('/user-groups/batch-delete', {ids});
    }
}

export default UserService;