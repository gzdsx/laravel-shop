import ApiService from "./ApiService";

const MaterialService = {
    list(params) {
        return ApiService.get('/materials', {params});
    },
    get(id) {
        return ApiService.get('/materials/' + id);
    },
    update(id, material) {
        return ApiService.put('/materials/' + id, {material});
    },
    batchDelete(ids) {
        return ApiService.post('/materials/batch-delete', {ids});
    }
}

export default MaterialService;