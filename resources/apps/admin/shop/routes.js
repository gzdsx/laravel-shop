import ShopList from "./ShopList";
import ShopEdit from "./ShopEdit";


module.exports = [
    {path: '/shop/list', component: ShopList, meta: {title: '门店管理'}},
    {path: '/shop/edit/:shop_id?', component: ShopEdit, meta: {title: '编辑门店'}},
]
