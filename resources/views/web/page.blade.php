@extends('layouts.default')

@section('title', $page->title)

@section('content')
    <div class="container">
        <div class="page-section">
            <div class="p-4">
                <h2 class="text-center">{{$page->title}}</h2>
                <div class="mt-5">{!! $page->content !!}</div>
            </div>
        </div>
    </div>
@stop