<?php

namespace App\ModelFilters;

use App\Models\ProductItem;
use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;

class ProductItemFilter extends ModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    public function cate($cate)
    {
        if ($cate) {
            if (!is_array($cate)) {
                $cate = explode(',', $cate);
            }

            //return $this->related('categories', 'cate_id', '=', [24, 25, 26]);
            return $this->whereHas('categories', function (Builder $builder) use ($cate) {
                return $builder->whereIn('cate_id', $cate);
            });
        }
        return $this;
    }

    /**
     * @param $q
     * @return ProductItemFilter
     */
    public function q($q)
    {
        return $this->where('title', 'like', "%$q%");
    }

    /**
     * @param $uid
     * @return ProductItemFilter
     */
    public function seller($seller_id)
    {
        return $this->where('seller_id', $seller_id);
    }

    /**
     * @param $name
     * @return ProductItemFilter|Builder
     */
    public function sellerName($name)
    {
        return $this->related('seller', 'nickname', 'like', "%$name%");
    }

    /**
     * @param $state
     * @return $this
     */
    public function status($status)
    {
        if ($status !== 'all') {
            return $this->where('status', $status);
        }
        return $this;
    }

    /**
     * @param $title
     * @return ProductItemFilter
     */
    public function title($title)
    {
        return $this->where('title', 'LIKE', "%$title%");
    }

    /**
     * @param $price
     * @return ProductItemFilter
     */
    public function minPrice($price)
    {
        return $this->where('price', '>', floatval($price));
    }

    /**
     * @param $price
     * @return $this|ProductItemFilter
     */
    public function maxPrice($price)
    {
        return $this->where('price', '<', floatval($price));
    }

    /**
     * @param $itemid
     * @return ProductItemFilter
     */
    public function product($product)
    {
        return $this->where('id', $product);
    }

    /**
     * @param $sort
     * @return $this|ProductItemFilter
     */
    public function sort($sort)
    {
        if ($sort == 'sale-desc') {
            return $this->orderByDesc('sold');
        }

        if ($sort == 'price-asc') {
            return $this->orderBy('price');
        }

        return $this->orderByDesc('id');
    }

    /**
     * @param $tab
     * @return $this|ProductItemFilter
     */
    public function tab($tab)
    {
        if ($tab == 'for-sale') {
            return $this->where('status', ProductItem::STAUTS_FOR_SALE);
        }

        if ($tab == 'off-sale') {
            return $this->where('status', ProductItem::STAUTS_OFF_SALE);
        }

        if ($tab == 'sold-out') {
            return $this->where('stock', 0);
        }
        return $this;
    }
}
