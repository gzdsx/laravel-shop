<?php

namespace App\Traits\RestApis;

use App\Models\FreightTemplate;
use Illuminate\Http\Request;

trait FreightTemplateApis
{
    /**
     * @return FreightTemplate|\Illuminate\Database\Eloquent\Builder
     */
    protected function repository()
    {
        return FreightTemplate::query();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        return json_success(['items' => $this->repository()->get()]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $model = $this->repository()->find($id);
        return json_success($model);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->repository()->whereKey($id)->delete();
        return json_success();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $id = null)
    {
        $model = $this->repository()->findOrNew($id);
        $model->fill($request->input('template', []))->save();
        return json_success($model);
    }
}