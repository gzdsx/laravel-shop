<?php

namespace App\Traits\RestApis;

use App\Models\ProductAttr;
use Illuminate\Http\Request;

trait ProductAttrApis
{
    /**
     * @return ProductAttr|\Illuminate\Database\Eloquent\Builder
     */
    protected function repository()
    {
        return ProductAttr::query();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $items = $this->repository()->get();
        return json_success([
            'total' => $items->count(),
            'items' => $items
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $model = $this->repository()->findOrFail($id);
        return json_success($model);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $id)
    {
        $model = $this->repository()->findOrNew($id);
        $model->fill($request->input('attr', []))->save();

        return json_success($model);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $model = $this->repository()->findOrFail($id);
        $model->delete();

        return json_success();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAttrValue(Request $request)
    {
        $attr_title = $request->input('attr_title');
        $attr_value = $request->input('attr_value');

        $attr = $this->repository()->firstOrCreate(['attr_title' => $attr_title]);
        $value = $attr->attrValues()->firstOrCreate(['attr_value' => $attr_value]);

        return json_success($value);
    }
}