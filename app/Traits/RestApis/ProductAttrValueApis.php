<?php

namespace App\Traits\RestApis;

use App\Models\ProductAttr;
use App\Models\ProductAttrValue;
use Illuminate\Http\Request;

trait ProductAttrValueApis
{
    /**
     * @return ProductAttrValue|\Illuminate\Database\Eloquent\Builder
     */
    protected function repository()
    {
        return ProductAttrValue::query();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $model = ProductAttr::findOrFail($request->input('attr_cate_id'));
        $model->load(['attrValues']);

        return json_success($model);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $id = null)
    {
        $model = $this->repository()->findOrNew($id);
        $model->fill($request->input('attr_value', []))->save();

        return json_success($model);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $model = $this->repository()->findOrFail($id);
        $model->delete();

        return json_success();
    }
}