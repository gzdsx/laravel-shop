<?php

namespace App\Traits\RestApis;

use App\Models\Block;
use App\Models\BlockItem;
use Illuminate\Http\Request;

trait BlockItemApis
{
    /**
     * @return BlockItem|\Illuminate\Database\Eloquent\Builder
     */
    protected function repository()
    {
        return BlockItem::query();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {

        $block = Block::findOrFail($request->input('block_id'));

        return json_success([
            'block' => $block,
            'items' => $block->items
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $model = $this->repository()->findOrFail($id);
        return json_success($model);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $id = null)
    {
        $newItem = $request->input('item', []);
        $model = $this->repository()->findOrNew($id);
        $model->fill($newItem)->save();
        return json_success($model);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function batchDelete(Request $request)
    {
        $this->repository()->whereKey($request->input('ids', []))->delete();
        return json_success();
    }
}