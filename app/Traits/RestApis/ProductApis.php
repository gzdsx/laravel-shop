<?php

namespace App\Traits\RestApis;

use App\Models\ProductItem;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

trait ProductApis
{
    /**
     * @return Builder|ProductItem
     */
    protected function repository()
    {
        return ProductItem::withGlobalScope('available', function (Builder $builder) {
            return $builder->where('status', ProductItem::STAUTS_FOR_SALE);
        });
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);
        $query = $this->repository()->filter($request->all());

        return json_success([
            'total' => $query->count(),
            'items' => $query->offset($offset)->take($limit)->get()
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $model = $this->repository()->findOrFail($id);
        $model->incrementViews();
        $model->load(['skus', 'content']);
        return json_success($model);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(Request $request, $id = null)
    {
        $product = DB::transaction(function () use ($request, $id) {
            $newProduct = collect($request->input('product', []));
            $model = $this->repository()->findOrNew($id);
            $model->fill($newProduct->toArray());
            if (!$model->seller_id) {
                $model->seller()->associate(Auth::id());
            }

            if (!$model->has_sku_attr) {
                $model->attrs = [];
            }
            $model->save();

            if ($newProduct->has('content')) {
                $content = $model->content()->firstOrNew();
                $content->fill($newProduct->get('content', []));
                $content->save();
            }

            if ($newProduct->has('categories')) {
                $model->categories()->sync($newProduct->get('categories', []));
            }

            if ($newProduct->has('images')) {
                $images = collect($newProduct->get('images', []));
                $model->images()->whereNotIn('id', $images->pluck('id'))->delete();

                foreach ($images as $k => $v) {
                    $v['sort_num'] = $k;
                    $model->images()->updateOrCreate(['id' => $v['id'] ?? 0], $v);
                }

                if ($first = $images->first()) {
                    $model->image = $first['image'] ?? null;
                    $model->save();
                }
            }

            if ($model->has_sku_attr) {
                if ($newProduct->has('skus')) {
                    $skus = $newProduct->get('skus', []);
                    $model->skus()->whereNotIn('sku_id', collect($skus)->pluck('sku_id'))->delete();
                    foreach ($skus as $sku) {
                        $sku['pin_price'] = $sku['pin_price'] ?? 0;
                        $model->skus()->updateOrCreate(['sku_id' => $sku['sku_id'] ?? 0], $sku);
                    }
                }
            } else {
                $model->skus()->delete();
            }

            if ($newProduct->has('metas')) {
                foreach ($newProduct->get('metas', []) as $meta_key => $meta_value) {
                    $model->updateMeta($meta_key, $meta_value);
                }
            }

            $model->refresh();
            return $model;
        });

        return json_success($product);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function batchDelete(Request $request)
    {
        $ids = $request->input('ids');
        if (is_array($ids)) {
            $this->repository()->whereKey($ids)->get()->each->delete();
        } else {
            if ($model = $this->repository()->find($request->input('ids'))) {
                $model->delete();
            }
        }
        return json_success();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function batchUpdate(Request $request)
    {
        $ids = $request->input('ids', []);
        $data = $request->input('data', []);
        foreach ($this->repository()->whereKey($ids)->get() as $product) {
            $product->fill($data)->save();
        }

        return json_success();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggle(Request $request)
    {
        $product = $this->repository()->find($request->input('id'));
        $product->isForSale() ? $product->markAsOffSale() : $product->markAsForSale();

        return json_success();
    }
}