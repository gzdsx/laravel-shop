<?php

namespace App\Http\Controllers\Web;

use App\Models\ProductItem;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ProductController extends BaseController
{
    /**
     * @return ProductItem|Builder
     */
    protected function repository()
    {
        return ProductItem::withGlobalScope('avalaible', function (Builder $builder) {
            return $builder->where('status', ProductItem::STAUTS_FOR_SALE);
        });
    }

    public function show(Request $request, $id)
    {
        $product = ProductItem::findOrFail($id);
        if ($product->status !== ProductItem::STAUTS_FOR_SALE) {
            abort(404, __('product.this product has been removed'));
        }

        $product->incrementViews();
        $product->load(['content', 'seller', 'images', 'skus', 'metas']);
        $reviews = $product->reviews()->paginate();
        $hotSales = $this->repository()->orderByDesc('sold')->limit(5)->get();

        return $this->view('web.product-detail', compact('product', 'reviews', 'hotSales'));
    }
}
