<?php

namespace App\Http\Controllers\Web;

use App\Models\ProductItem;
use Illuminate\Http\Request;

class ShopController extends BaseController
{
    protected $navName = 'shop';

    public function index(Request $request)
    {
        $products = ProductItem::query()->orderByDesc('id')->limit(15)->get();

        return $this->view('web.shop-home', compact('products'));
    }
}
