<?php

namespace App\Http\Controllers\Web;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends BaseController
{
    public function show(Request $request, $id)
    {
        $page = Page::whereName($id)->first();

        return $this->view('web.page', compact('page'));
    }
}
