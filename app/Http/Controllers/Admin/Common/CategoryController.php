<?php

namespace App\Http\Controllers\Admin\Common;

use App\Http\Controllers\Admin\BaseController;
use App\Traits\RestApis\CategoryApis;

class CategoryController extends BaseController
{
    use CategoryApis;
}
