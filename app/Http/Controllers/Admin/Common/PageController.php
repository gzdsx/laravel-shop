<?php

namespace App\Http\Controllers\Admin\Common;

use App\Http\Controllers\Admin\BaseController;
use App\Traits\RestApis\PageApis;

class PageController extends BaseController
{
    use PageApis;
}
