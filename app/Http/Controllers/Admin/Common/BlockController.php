<?php

namespace App\Http\Controllers\Admin\Common;


use App\Http\Controllers\Admin\BaseController;
use App\Traits\RestApis\BlockApis;

class BlockController extends BaseController
{
    use BlockApis;
}
