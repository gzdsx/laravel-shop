<?php

namespace App\Http\Controllers\Admin\Common;

use App\Http\Controllers\Admin\BaseController;
use App\Traits\RestApis\FreightTemplateApis;

class FreightTemplateController extends BaseController
{
    use FreightTemplateApis;
}
