<?php

namespace App\Http\Controllers\Admin\Common;

use App\Http\Controllers\Admin\BaseController;
use App\Traits\RestApis\MenuItemApis;

class MenuItemController extends BaseController
{
    use MenuItemApis;
}
