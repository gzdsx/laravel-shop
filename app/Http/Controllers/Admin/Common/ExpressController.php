<?php

namespace App\Http\Controllers\Admin\Common;


use App\Http\Controllers\Admin\BaseController;
use App\Traits\RestApis\ExpressApis;

class ExpressController extends BaseController
{
    use ExpressApis;
}
