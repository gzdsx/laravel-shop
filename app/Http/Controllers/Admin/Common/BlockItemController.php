<?php

namespace App\Http\Controllers\Admin\Common;

use App\Http\Controllers\Admin\BaseController;
use App\Traits\RestApis\BlockItemApis;

class BlockItemController extends BaseController
{
    use BlockItemApis;
}
