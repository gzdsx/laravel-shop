<?php

namespace App\Http\Controllers\Admin\Common;


use App\Http\Controllers\Admin\BaseController;
use App\Traits\RestApis\AdApis;

class AdController extends BaseController
{
    use AdApis;
}
