<?php

namespace App\Http\Controllers\Admin\Common;

use App\Http\Controllers\Admin\BaseController;
use App\Traits\RestApis\MenuApis;

class MenuController extends BaseController
{
    use MenuApis;
}
