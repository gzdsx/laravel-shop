<?php

namespace App\Http\Controllers\Admin\Shop;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Shop;
use App\Traits\RestApis\ShopApis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopController extends BaseController
{
    use ShopApis;

    /**
     * @return Shop|\Illuminate\Database\Eloquent\Builder
     */
    protected function repository()
    {
        return Shop::query();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify(Request $request, $id)
    {
        $shop = $this->repository()->find($id);
        $shop->auth_state = $request->input('auth_state', 0);
        $shop->save();

        return json_success();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function batchUpdate(Request $request)
    {
        $this->repository()->whereKey($request->input('ids', []))->update($request->input('data', []));
        return json_success();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $id = null)
    {
        $model = $this->repository()->findOrNew($id);
        $model->fill($request->input('shop', []));

        if (!$model->seller_id) {
            $model->seller()->associate(Auth::id());
        }

        $model->save();

        $newShop = collect($request->input('shop', []));
        if ($newShop->has('images')) {
            $images = $newShop->get('images', []);
            $model->images()->whereNotIn('id', collect($images)->pluck('id'))->delete();

            foreach ($images as $k => $v) {
                $v['sort_num'] = $k;
                $model->images()->updateOrCreate(['id' => $v['id'] ?? 0], $v);
            }
        }

        return json_success();
    }
}
