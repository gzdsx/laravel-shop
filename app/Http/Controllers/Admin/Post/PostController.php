<?php

namespace App\Http\Controllers\Admin\Post;

use App\Http\Controllers\Controller;
use App\Models\PostItem;
use App\Traits\RestApis\PostApis;

class PostController extends Controller
{
    use PostApis;

    /**
     * @return PostItem|\Illuminate\Database\Eloquent\Builder
     */
    protected function repository()
    {
        return PostItem::query();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $model = $this->repository()->findOrNew($id);
        $model->incrementViews();
        $model->load(['user', 'images', 'media', 'categories', 'metas']);

        return json_success($model);
    }
}
