<?php

namespace App\Http\Controllers\Admin\User;


use App\Http\Controllers\Admin\BaseController;
use App\Models\UserTransaction;
use App\Traits\RestApis\UserTransactionApis;

class TransactionController extends BaseController
{
    use UserTransactionApis;

    protected function repository()
    {
        return UserTransaction::query();
    }
}
