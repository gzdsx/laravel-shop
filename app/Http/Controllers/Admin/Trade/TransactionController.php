<?php

namespace App\Http\Controllers\Admin\Trade;

use App\Http\Controllers\Admin\BaseController;
use App\Models\UserTransaction;
use App\Traits\RestApis\UserTransactionApis;

class TransactionController extends BaseController
{
    use UserTransactionApis;
    /**
     * @return UserTransaction|\Illuminate\Database\Eloquent\Builder
     */
    protected function repository()
    {
        return UserTransaction::query();
    }
}
