<?php

namespace App\Http\Controllers\Admin\Trade;


use App\Http\Controllers\Admin\BaseController;
use App\Traits\RestApis\RefundTrait;

class RefundController extends BaseController
{
    use RefundTrait;
}
