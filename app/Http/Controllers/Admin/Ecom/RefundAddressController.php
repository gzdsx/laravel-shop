<?php

namespace App\Http\Controllers\Admin\Ecom;

use App\Http\Controllers\Admin\BaseController;
use App\Traits\Ecom\RefundAddressTrait;

class RefundAddressController extends BaseController
{
    use RefundAddressTrait;
}
