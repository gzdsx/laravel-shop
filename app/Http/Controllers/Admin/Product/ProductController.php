<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Admin\BaseController;
use App\Models\ProductItem;
use App\Traits\RestApis\ProductApis;

class ProductController extends BaseController
{
    use ProductApis;

    /**
     * @return ProductItem|\Illuminate\Database\Eloquent\Builder
     */
    protected function repository()
    {
        return ProductItem::query();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $model = $this->repository()->findOrNew($id);
        $model->incrementViews();
        $model->load(['skus', 'content', 'categories']);
        return json_success($model);
    }
}
