<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Admin\BaseController;
use App\Traits\RestApis\ProductAttrApis;

class ProductAttrController extends BaseController
{
    use ProductAttrApis;
}
