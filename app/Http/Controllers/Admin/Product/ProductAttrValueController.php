<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Admin\BaseController;
use App\Traits\RestApis\ProductAttrValueApis;

class ProductAttrValueController extends BaseController
{
    use ProductAttrValueApis;
}
