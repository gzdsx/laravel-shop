<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\BoughtApis;

class OrderController extends BaseController
{
    use BoughtApis;
}
