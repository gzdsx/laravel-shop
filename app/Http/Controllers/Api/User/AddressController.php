<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\UserAddressApis;

class AddressController extends BaseController
{
    use UserAddressApis;
}
