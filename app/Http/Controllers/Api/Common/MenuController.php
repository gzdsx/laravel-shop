<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\MenuApis;

class MenuController extends BaseController
{
    use MenuApis;
}
