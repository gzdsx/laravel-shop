<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\ShortcutApis;

class ShortcutController extends BaseController
{
    use ShortcutApis;
}
