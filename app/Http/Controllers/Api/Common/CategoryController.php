<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\CategoryApis;

class CategoryController extends BaseController
{
    use CategoryApis;
}
