<?php

namespace App\Http\Controllers\Api\Common;


use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\PageApis;

class PageController extends BaseController
{
    use PageApis;
}
