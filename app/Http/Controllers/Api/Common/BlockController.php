<?php

namespace App\Http\Controllers\Api\Common;


use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\BlockApis;

class BlockController extends BaseController
{
    use BlockApis;
}
