<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\AdApis;

class AdController extends BaseController
{
    use AdApis;
}
