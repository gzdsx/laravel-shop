<?php

namespace App\Http\Controllers\Api\Common;


use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\ExpressApis;

class ExpressController extends BaseController
{
    use ExpressApis;
}
