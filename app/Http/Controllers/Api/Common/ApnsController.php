<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Api\BaseController;
use App\Models\JPush;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApnsController extends BaseController
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function jpush(Request $request)
    {
        $client_id = $request->header('x-gzdsx-client-id');
        $os = $request->input('os', 'ios');
        $registerid = $request->input('registerid');
        if ($os && $registerid) {
            $jpush = JPush::firstOrNew(['registerid' => $registerid]);
            $jpush->client_id = $client_id;
            $jpush->os = $os;
            if (Auth::check()) {
                $jpush->user_id = Auth::id();
            }
            $jpush->save();

            return json_success($jpush);
        }

        return json_fail('registerid empty');
    }
}
