<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\MaterialApis;

class MaterialController extends BaseController
{
    use MaterialApis;
}
