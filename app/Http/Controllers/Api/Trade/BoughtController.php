<?php

namespace App\Http\Controllers\Api\Trade;

use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\BoughtApis;

class BoughtController extends BaseController
{
    use BoughtApis;
}
