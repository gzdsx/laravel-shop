<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\ShopApis;

class ShopController extends BaseController
{
    use ShopApis;
}
