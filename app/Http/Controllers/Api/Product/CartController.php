<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\CartApis;

class CartController extends BaseController
{
    use CartApis;
}
