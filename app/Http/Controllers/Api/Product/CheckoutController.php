<?php

namespace App\Http\Controllers\Api\Product;

use App\Events\Order\OrderCreated;
use App\Http\Controllers\Api\BaseController;
use App\Models\Order;
use App\Models\ProductGroupItem;
use App\Models\ProductItem;
use App\Support\TradeUtil;
use App\Traits\Ecom\HasFreight;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CheckoutController extends BaseController
{
    use HasFreight;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculate(Request $request)
    {
        $sku_id = $request->input('sku_id', 0);
        $quantity = $request->input('quantity', 1);
        $is_pin = $request->input('is_pin', 0);
        $product_id = $request->input('product_id');
        $product = ProductItem::findOrFail($product_id);

        if (!$sku = $product->skus()->find($sku_id)) {
            $sku = $product->skus()->make([
                'stock' => $product->stock,
                'price' => $product->price,
                'pin_price' => $product->pin_price
            ]);
        }

        if ($quantity > $sku->stock) {
            abort(403, trans('product.insufficient inventory'));
        }

        $product_fee = bcmul($is_pin ? $sku->pin_price : $sku->price, $quantity, 2);
        $shipping_fee = $this->computeFreight($product->template_id, $quantity, $product_fee);
        $discount_fee = '0.00';
        return json_success([
            'sku' => $sku,
            'product' => $product,
            'quantity' => $quantity,
            'product_fee' => $product_fee,
            'shipping_fee' => bcadd($shipping_fee, 0, 2),
            'discount_fee' => bcadd($discount_fee, 0, 2),
            'order_fee' => bcsub(bcadd($product_fee, $shipping_fee, 2), $discount_fee, 2)
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function createOrder(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $sku_id = $request->input('sku_id', 0);
            $quantity = $request->input('quantity', 1);
            $pay_type = $request->input('pay_type', 1);
            $group_id = $request->input('group_id', 0);
            $order_type = $request->input('order_type', 1);
            $product_id = $request->input('product_id');

            $product = ProductItem::findOrFail($product_id);
            if (!$sku = $product->skus()->find($sku_id)) {
                $sku = $product->skus()->make([
                    'stock' => $product->stock,
                    'price' => $product->price,
                    'pin_price' => $product->pin_price
                ]);
            }

            if ($quantity > $sku->stock) {
                abort(500, trans('product.insufficient inventory'));
            }

            $product_fee = bcmul($order_type == 2 ? $sku->pin_price : $sku->price, $quantity, 2);
            $shipping_fee = $this->computeFreight($product->template_id, $quantity, $product_fee);
            $discount_fee = '0.00';

            $order = new Order();
            $order->order_no = TradeUtil::createOrderNo();
            $order->out_trade_no = TradeUtil::createOutTradeNo();
            $order->order_status = Order::ORDER_STATUS_UNPAID;
            $order->order_type = $order_type;
            $order->pay_type = $pay_type;
            $order->product_fee = $product_fee;
            $order->shipping_fee = $shipping_fee;
            $order->discount_fee = $discount_fee;
            $order->remark = $request->input('remark');

            if ($buyer = Auth::user()) {
                $order->buyer_id = $buyer->uid;
                $order->buyer_name = $buyer->nickname;
            }

            if ($shop = $product->shop) {
                $order->shop_id = $shop->shop_id;
                $order->shop_name = $shop->shop_name;
            }

            if ($seller = $product->seller) {
                $order->seller_id = $seller->uid;
                $order->seller_name = $seller->nickname;
            }

            $order->save();

            $order->items()->create([
                'product_id' => $product->id,
                'title' => $product->title,
                'price' => $order_type == 2 ? $sku->pin_price : $sku->price,
                'image' => $product->image,
                'quantity' => $quantity,
                'sku_id' => $sku->sku_id ?? 0,
                'sku_title' => $sku->title
            ]);

            //添加操作记录
            $order->logs()->create([
                'uid' => $order->buyer_id,
                'nickname' => $order->buyer_name,
                'content' => trans('trade.order create success')
            ]);

            //更新收货地址
            if ($address = $request->input('address')) {
                $order->shipping->fill($address)->save();
            }

            if ($order_type == 2) {
                if ($group_id) {
                    $is_chief = 0;
                    if (ProductGroupItem::where(['group_id' => $product, 'user_id' => Auth::id()])->exists()) {
                        abort(500, '不能重复参团');
                    }
                } else {
                    $is_chief = 1;
                    $group = $product->groups()->create([
                        'user_id' => $buyer->uid,
                        'required_num' => $product->pin_num ?: 1
                    ]);
                }

                $group->items()->create([
                    'user_id' => $buyer->uid,
                    'product_id' => $product_id,
                    'order_id' => $order->order_id,
                    'is_chief' => $is_chief
                ]);
            }

            event(new OrderCreated($order));
            return json_success($order);
        });
    }
}
