<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Api\BaseController;
use App\Traits\RestApis\ProductApis;

class ProductController extends BaseController
{
    use ProductApis;
}
