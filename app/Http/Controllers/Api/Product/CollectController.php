<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Api\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CollectController extends BaseController
{
    /**
     * @return \App\Models\ProductItem|\Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    protected function repository(){
        return Auth::user()->collectedProducts();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $query = $this->repository();

        return json_success([
            'total' => $query->count(),
            'items' => $query->offset($request->input('offset', 0))
                ->limit($request->input('limit', 10))->get()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function query(Request $request)
    {
        $product_id = $request->input('product_id');
        $exists = $this->repository()->whereKey($product_id)->exists();

        return json_success($exists);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggle(Request $request)
    {
        $product_id = $request->input('product_id');
        $res = $this->repository()->toggle([$product_id]);

        return json_success($res);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove(Request $request)
    {
        $product_id = $request->input('product_id');
        $res = $this->repository()->detach([$product_id]);

        return json_success($res);
    }
}
