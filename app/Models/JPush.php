<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\CommonJPush
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $client_id
 * @property string|null $os
 * @property string|null $registerid
 * @property-read \App\Models\User|null $user
 * @method static Builder|JPush newModelQuery()
 * @method static Builder|JPush newQuery()
 * @method static Builder|JPush query()
 * @method static Builder|JPush whereClientId($value)
 * @method static Builder|JPush whereId($value)
 * @method static Builder|JPush whereOs($value)
 * @method static Builder|JPush whereRegistrationid($value)
 * @method static Builder|JPush whereUserId($value)
 * @mixin \Eloquent
 */
class JPush extends Model
{
    protected $table = 'jpush';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'client_id', 'os', 'registerid'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'uid');
    }
}
