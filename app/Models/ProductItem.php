<?php

namespace App\Models;

use App\Models\Traits\HasDates;
use App\Models\Traits\HasImageAttribute;
use App\Models\Traits\HasMetas;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


/**
 * App\Models\ProductItem
 *
 * @property int $id 商品ID
 * @property int $seller_id 用户ID
 * @property int $shop_id 门店ID
 * @property string|null $title 宝贝标题
 * @property string|null $subtitle 宝贝卖点
 * @property string|null $merchant_code 商品编号
 * @property string $image 商品图片
 * @property string|null $price 一口价
 * @property string|null $original_price 商品原价
 * @property string|null $promotion_price 促销价
 * @property int $purchase_limit 限购数量
 * @property int $sold 销量
 * @property int $stock 库存
 * @property int $views 浏览量
 * @property int $collect_num 收藏数量
 * @property int $comment_num 评论数
 * @property array|null $attrs 商品属性
 * @property int $is_new 是否新品
 * @property bool $is_hot 是否热销
 * @property int $is_recommend 仓储推荐
 * @property int $is_promotion 是否促销
 * @property int $is_top 是否置顶
 * @property int $free_delivery 免运费
 * @property int $template_id 运费模板
 * @property int $is_weight_template 是否按重量计价
 * @property int $has_sku_attr 是否有多级型号
 * @property int $brand_id 品牌
 * @property string $status 商品状态
 * @property \Illuminate\Support\Carbon|null $created_at 创建时间
 * @property \Illuminate\Support\Carbon|null $updated_at 更新时间
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Category> $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $collectedUsers
 * @property-read int|null $collected_users_count
 * @property-read \App\Models\ProductContent|null $content
 * @property-read array|string|null $status_des
 * @property-read \Illuminate\Contracts\Routing\UrlGenerator|string $url
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\ProductGroup> $groups
 * @property-read int|null $groups_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\ProductImage> $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\ProductMeta> $metas
 * @property-read int|null $metas_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\ProductReview> $reviews
 * @property-read int|null $reviews_count
 * @property-read \App\Models\User|null $seller
 * @property-read \App\Models\Shop|null $shop
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\ProductSku> $skus
 * @property-read int|null $skus_count
 * @property-read \App\Models\FreightTemplate|null $template
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem filter(array $input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem simplePaginateFilter(?int $perPage = null, ?int $columns = [], ?int $pageName = 'page', ?int $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereAttrs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereBeginsWith(string $column, string $value, string $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereCollectNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereCommentNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereEndsWith(string $column, string $value, string $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereFreeDelivery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereHasSkuAttr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereIsHot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereIsNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereIsPromotion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereIsRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereIsTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereIsWeightTemplate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereLike(string $column, string $value, string $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereMerchantCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereOriginalPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem wherePromotionPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem wherePurchaseLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereSellerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereSold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereStock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereTemplateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductItem whereViews($value)
 * @mixin \Eloquent
 */
class ProductItem extends Model
{
    use HasDates, HasImageAttribute, HasMetas, Filterable;

    const STAUTS_FOR_SALE = 'for-sale';
    const STAUTS_OFF_SALE = 'off-sale';
    const STAUTS_SOLD_OUT = 'sold-out';

    protected $table = 'product_item';
    protected $primaryKey = 'id';
    protected $fillable = [
        'shop_id', 'cate_id', 'title', 'subtitle', 'merchant_code', 'image', 'price',
        'purchase_limit', 'original_price', 'promotion_price', 'sold', 'stock', 'views',
        'collect_num', 'comment_num', 'attrs', 'is_recommend', 'is_promotion', 'is_top',
        'free_delivery', 'template_id', 'is_weight_template', 'has_sku_attr',
        'is_hot', 'is_new', 'brand_id', 'status'
    ];
    protected $hidden = ['seller_id'];
    protected $appends = ['url', 'status_des'];
    protected $casts = [
        'attrs' => 'array',
        'is_hot' => 'boolean'
    ];
    protected $with = ['shop', 'seller', 'images', 'metas'];

    public static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
        static::creating(function (ProductItem $productItem) {
            if (!$productItem->seller_id) {
                $productItem->seller()->associate(Auth::id());
            }
        });

        static::deleting(function (ProductItem $productItem) {
            $productItem->content()->delete();
            $productItem->images()->delete();
            $productItem->reviews->each->delete();
        });
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getUrlAttribute()
    {
        return $this->id ? url('product/' . $this->id . '.html') : null;
    }

    /**
     * @return array|string|null
     */
    public function getStatusDesAttribute()
    {
        return is_null($this->status) ? null : trans('product.statuses.' . $this->status);
    }

    /**
     * @param int $amount
     * @return int
     */
    public function incrementSold($amount = 1)
    {
        return $this->increment('sold', $amount);
    }

    /**
     * @param int $amount
     * @return int
     */
    public function incrementViews($amount = 1)
    {
        return $this->increment('views', $amount);
    }

    /**
     * @param $amount
     * @return int
     */
    public function decreaseStock($amount)
    {
        return $this->newQuery()->where('id', $this->id)
            ->where('stock', '>=', $amount)
            ->decrement('stock', $amount);
    }


    /**
     * 宝贝所属用户
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seller()
    {
        return $this->belongsTo(User::class, 'seller_id', 'uid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id', 'shop_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|ProductContent
     */
    public function content()
    {
        return $this->hasOne(ProductContent::class, 'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(ProductImage::class, 'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|ProductSku
     */
    public function skus()
    {
        return $this->hasMany(ProductSku::class, 'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany(ProductReview::class, 'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function template()
    {
        return $this->belongsTo(FreightTemplate::class, 'template_id', 'template_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups()
    {
        return $this->hasMany(ProductGroup::class, 'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(
            Category::class,
            'product_category',
            'product_id',
            'product_cate_id',
            'id',
            'cate_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|User
     */
    public function collectedUsers()
    {
        return $this->belongsToMany(
            User::class,
            'product_collect',
            'product_id',
            'user_id',
            'id',
            'uid'
        )->as('collect')
            ->withTimestamps()
            ->orderBy('product_collect.created_at', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|ProductMeta
     */
    public function metas()
    {
        return $this->hasMany(ProductMeta::class, 'product_id', 'id');
    }

    /**
     * @return bool
     */
    public function isForSale()
    {
        return $this->status == self::STAUTS_FOR_SALE;
    }

    public function isOffSale()
    {
        return $this->status == self::STAUTS_OFF_SALE;
    }

    public function isSoldOut()
    {
        return $this->status == self::STAUTS_SOLD_OUT;
    }

    public function markAsForSale()
    {
        return $this->forceFill(['status' => self::STAUTS_FOR_SALE])->save();
    }

    public function markAsOffSale()
    {
        return $this->forceFill(['status' => self::STAUTS_OFF_SALE])->save();
    }

    public function markAsSoldOut()
    {
        return $this->forceFill(['status' => self::STAUTS_SOLD_OUT])->save();
    }
}
