<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\CommonSetting
 *
 * @property string $skey 标识
 * @property string|null $svalue 值
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereSkey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereSvalue($value)
 * @mixin \Eloquent
 */
class Setting extends Model
{
    protected $table = 'setting';
    protected $primaryKey = 'skey';
    protected $keyType = 'string';
    protected $fillable = ['skey', 'svalue'];

    public $timestamps = false;
    public $incrementing = false;

    /**
     * @return void
     * @throws \Exception
     */
    public static function updateCache()
    {
        $settings = [];
        foreach (Setting::all() as $setting) {
            $svalue = json_decode($setting->svalue, true);
            $settings[$setting->skey] = is_array($svalue) ? $svalue : $setting->svalue;
        }
        cache()->forever('settings', $settings);
    }

    /**
     * @return \Illuminate\Contracts\Cache\Repository|mixed
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function settingsFromCache()
    {
        $settings = cache()->get('settings');
        if (is_null($settings)) {
            Setting::updateCache();
            return cache()->get('settings');
        }

        return $settings;
    }
}
